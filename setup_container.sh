#!/bin/bash

# Setup unprivileged containers
# This scripts is used in ansible playbook

# Variables
  container_name="$1"
  user="test"
  user_passwd='$6$RDDaoI5Eys3Q$cTJPB4sPCEGbETaKqhM7VEkFpw.QyknqYrFZi9YYhSN1XlKJ5/d8EuIjexam3xWGK/8IApSTl0SqL3Zi5sb8b.' # mkpasswd -m sha-512 -S saltsalt -s <<< YourPasswd
  ssh_auth_key='ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGg8GqEcZHdf/LXmQx1SELauH/Q+UkbFOKo7se4z2p1k'

# Createing lock file
  lock_file=/tmp/setup_container.lock
  if [ -f "$lock_file" ]; then
     echo "[-] Script is already running"
     exit 1
  fi
  touch $lock_file

# Delete lock file at exit
  trap "rm -rf $lock_file" EXIT

# Test if argument given
  if [ -z $1 ];then
    echo "No container name"
    exit
  fi

# Test if container exists
  if [ "$(lxc-ls -f | grep $container_name | cut -d ' ' -f1)" != $container_name ];then
     echo "The container $container_name does not exists"
     exit
  fi

# Create user $user, add to sudoers, set color yellow
lxc-attach -n $container_name -- bash -c "useradd -m -p '${user_passwd}' -s /bin/bash $user"
lxc-attach -n $container_name -- bash -c "usermod -aG sudo $user"
lxc-attach -n $container_name -- bash -c "sed -i.bck -e s'/32m/33m/g' /home/$user/.bashrc "

# Install python2.7 and python-simplejson for ansible
lxc-attach -n $container_name -- bash -c "apt -y install python2.7"
lxc-attach -n $container_name -- bash -c "apt -y install python-simplejson"

# Install ssh and nano editor
lxc-attach -n $container_name -- bash -c "apt -y install ssh"
lxc-attach -n $container_name -- bash -c "apt -y install nano"

# Add ssh key
lxc-attach -n $container_name -- bash -c "mkdir /home/$user/.ssh"
lxc-attach -n $container_name -- bash -c "echo '$ssh_auth_key' >> /home/$user/.ssh/authorized_keys"
lxc-attach -n $container_name -- bash -c "chown -R $user:$user /home/$user/.ssh"

exit
