# lxc_scripts
Scripts that I have used with LXC containers. These scripts are for LXC1 (not LXD client). They are no 'ready to use' I share them because they could inspire someone.

### set_unprivileged_containers.sh
This script set enviorement to run unprivileged LXC containers as normal user.
Check code to see how it works. This scripts is used in ansible playbook.

### create_container.sh
Create unprivileged containers. It uses the first argument as name for container.
It can be used in a loop to create multiple of them. This scripts is used in ansible playbook.

### setup_container.sh
Setup unprivileged containers. It uses the first argument as container to setup. It can be used in a loop to setup multiple of them. This scripts is used in ansible playbook.

### iptables_containers_port_forwarding.sh
Just to add PREROUTING rules to the server towards containers. It's also used in the adjoint ansible playbook.

### full_install_server.yml
It is a ansible role transformed in playbook, so you will have to work little to make it works. It uses all other scripts to setup unpriviledged containers. I created it because the module ansible_lxc doesn't support them and I wanted to create odoo containers. 
