#!/bin/bash

# Add PREROUTING rules to server

# Variables
IPT=$(which iptables)
IPT_SAVE=$(which iptables-save)

# Get active interface
interface=$(ls -1 /sys/class/net | grep -v lo | grep -v lxcbr0)

# Test if rule already exists before apply
if [[ ! $($IPT_SAVE | grep "10002 -j DNAT --to 10.0.10.2:22") ]];then
   $IPT -t nat -A PREROUTING -i $interface -p tcp --dport 10002 -j DNAT --to 10.0.10.2:22
fi

if [[ ! $($IPT_SAVE | grep "10003 -j DNAT --to 10.0.10.2:8069") ]];then
   $IPT -t nat -A PREROUTING -i $interface -p tcp --dport 10003 -j DNAT --to 10.0.10.2:8069
fi

exit

# Container
#$IPT -t nat -A PREROUTING -i enp0s3 -p tcp --dport 10002 -j DNAT --to 10.0.10.2:22
#$IPT -t nat -A PREROUTING -i enp0s3 -p tcp --dport 10003 -j DNAT --to 10.0.10.2:8069

