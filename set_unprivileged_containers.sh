#!/bin/bash

# Set enviorement to run unprivileged containers

# Colors and symbols
  txtred=$(tput setaf 1)  # red
  txtgrn=$(tput setaf 2)  # green
  txtylw=$(tput setaf 3)  # yellow
  txtbld=$(tput bold)     # text bold
  txtrst=$(tput sgr0)     # text reset

  ok="[ ${txtgrn}OK${txtrst} ]"
  fail="[${txtred}FAIL${txtrst}]"
  warn="[${txtylw}WARN${txtrst}]"
  wait="[${txtylw} -- ${txtrst}]"

# Variables
  unprivileged_user="$SUDO_USER"

# Createing lock file
  lock_file=/tmp/set_unprivileged_containers.lock
  if [ -f "$lock_file" ]; then
     echo "$fail The script is already running"
     exit 1
  fi
  touch $lock_file

# Delete lock file at exit
  trap "rm -rf $lock_file" EXIT

# Check root
  if [ $(id -u) != "0" ]; then
     echo -e "$fail You must be root to run this script"
     exit 1
  fi

# Check user
  if [ $(getent passwd $unprivileged_user) ];then
     echo -e "$ok Unprivileged user = $unprivileged_user"
  else
     echo -e "$fail Unprivileged user $unprivileged_user does not exist"
     exit 1
  fi

# Check lxc
  if [ $(command -v lxc-create) ];then
     echo -e "$ok Binary lxc-create found"
  else
     echo -e "$fail Binary lxc-create not found (is lxc installed?)"
     exit 1
  fi

# Set usernet quota
  if [ "$(cat /etc/lxc/lxc-usernet | grep ^$unprivileged_user )" ];then
     echo -e "$warn The user has already a quota in lxc-usernet"
  else
     echo -e "$ok Setting lxc-usernet quota"
     echo "$unprivileged_user veth lxcbr0 10" > /etc/lxc/lxc-usernet
  fi

# Create lxc directory
  if [ -d ~/.config/lxc ];then
     echo -e "$warn The ~/.config/lxc directory already exists"
  else
     echo -e "$ok ~/.config/lxc directory created"
     sudo -u "$unprivileged_user" mkdir -p ~/.config/lxc
     chmod 0755 ~/.config/lxc
  fi

# Copying default.conf
  if [ -r /etc/lxc/default.conf ];then
     if [ -r ~/.config/lxc/default.conf ];then
        read -n 1 -s -r -e -p "$wait The ~/.config/lxc/default.conf file already exists. Delete it? (y/n)" answer

        if [[ "$answer" =~ [Yy]$ ]];then
           echo -e "$ok Copying ~/.config/lxc/default.conf file"
           cp /etc/lxc/default.conf ~/.config/lxc/default.conf
           chown $unprivileged_user:$unprivileged_user ~/.config/lxc/default.conf
           chmod 0777 ~/.config/lxc/default.conf
        else
           echo "$fail Aborted"
           exit 1
        fi
     else
        echo -e "$ok Copying default.conf file"
        cp /etc/lxc/default.conf ~/.config/lxc/default.conf
        chown $unprivileged_user:$unprivileged_user ~/.config/lxc/default.conf
        chmod 0777 ~/.config/lxc/default.conf
     fi
  else
     echo -e "$fail default.conf file not found (is lxc installed?)"
     exit 1
  fi

# Adding x permissions to ~/.local directory
  if [ $(stat --format '%a' ~/.local) != 777 ];then
     echo "$ok Adding x permissions to ~/.local directory"
     chmod 777 ~/.local
  else
     echo "$warn The directory ~/.local already has x permissions"
  fi

# Adding x permissions to ~/.local/share directory
  if [ $(stat --format '%a' ~/.local/share) != 777 ];then
     echo "$ok Adding x permissions to ~/.local/share directory"
     chmod 777 ~/.local/share
  else
     echo "$warn The directory ~/.local/share already has x permissions"
  fi

# Getting subuid and subgid
  if [ $(cat /etc/subuid) ];then
     uid=$(cat /etc/subuid | grep $unprivileged_user | cut -d ':' -f2,3 | tr ':' ' ')
     echo -e "$ok Getting uid = $uid"
  else
     echo -e "$fail Error getting uid"
     exit 1
  fi

  if [ $(cat /etc/subgid) ];then
     gid=$(cat /etc/subgid | grep $unprivileged_user | cut -d ':' -f2,3 | tr ':' ' ')
     echo -e "$ok Getting gid = $gid"
  else
     echo -e "$fail Error getting gid"
     exit 1
  fi

# Write config file
  echo "lxc.id_map = u 0 $uid" >> ~/.config/lxc/default.conf
  echo "lxc.id_map = g 0 $gid" >> ~/.config/lxc/default.conf

  # Allow TUN dev for OpenVPN
    #echo "lxc.cgroup.devices.allow = c 10:200 rwm" >> ~/.config/lxc/default.conf

  if [ "$(cat ~/.config/lxc/default.conf | grep "lxc.id_map = u 0 $uid")" ] && \
     [ "$(cat ~/.config/lxc/default.conf | grep "lxc.id_map = g 0 $uid")" ];then
     echo "$ok Config file written"
  else
    echo "$fail config file not written"
    exit 1
  fi

# Show config
  echo
  echo "## USER NET QUOTA ##"
  cat /etc/lxc/lxc-usernet
  echo

  echo "## DIRECTORY PERMISSIONS ##"
  echo "~/.config/lxc = $(stat --format '%a' ~/.config/lxc)"
  echo "~/.local = $(stat --format '%a' ~/.local)"
  echo "~/.local/share = $(stat --format '%a' ~/.local/share)"
  echo

  echo "## CONFIG FILE ##"
  cat ~/.config/lxc/default.conf
  echo

echo "Just before you create your first container, you probably should logout and login again, or even reboot your machine to make sure that your user is placed in the right cgroups."
echo



exit
# Create container ?
  read -n 1 -s -r -e -p "$wait Do you want to create a container to test? (y/n)" answer

  if [[ "$answer" =~ [Yy]$ ]];then
     if [[ $(lxc-ls | grep "$container_name") ]];then
        echo "$fail The container already exists"
        exit 1
     else
        echo "$wait Creating container as user $unprivileged_user"
        sudo -u $unprivileged_user lxc-create --template download --name "ubuntu_test_container" -- \
                                              --dist ubuntu \
                                              --release xenial \
                                              --arch i386 \
                                              --keyserver hkp://p80.pool.sks-keyservers.net:80

        if [ $? = 0 ];then
           echo "$ok Container created"
           lxc-ls -f
           exit
        else
           echo "$fail The container creation fail"
           exit 1
        fi
     fi
  else
     exit
  fi

exit
