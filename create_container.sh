#!/bin/bash

# Create unprivileged containers
# This scripts is used in ansible playbook

# Variables
  container_name="$1"

# Createing lock file
  lock_file=/tmp/create_container.lock
  if [ -f "$lock_file" ]; then
     echo "Script is already running"
     exit 1
  fi
  touch $lock_file

# Delete lock file at exit
  trap "rm -rf $lock_file" EXIT

# Test if container exists
  if [ "$(lxc-ls -f | grep '$container_name' | cut -d ' ' -f1)" == '$container_name' ];then
     echo "The container $container_name already exists"
     exit
  fi

# Create container
  lxc-create --template download --name "$container_name" -- \
             --dist ubuntu \
             --release xenial \
             --arch amd64 \
             --keyserver hkp://p80.pool.sks-keyservers.net:80

exit
